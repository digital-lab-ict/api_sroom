import json
import logging
import sys

import smart_room

from flask import Flask, request
from flask.ext.cors import CORS

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return "api server v1.0"

@app.route("/smart-room/1.0/rooms", methods=['GET'])
def get_room_list():
    logging.info("get_room_status.1")
    ret_val = {}
    room_list = smart_room.SmartRoom.get_room_list()
    try:
        ret_val = {"status": "ok", 'room_list': room_list }
    except smart_room.SmartRoom.RoomNotFoundError as e:
        ret_val = {"status": "ko", 'error': e.error }

    return json.dumps(ret_val)

@app.route("/smart-room/1.0/rooms/<room_id>/data", methods=['GET'])
def get_room_status(room_id):
    logging.info("get_room_status.1")
    ret_val = {}
    offset = int(request.args.get("offset"))
    limit = int(request.args.get("limit"))
    room = smart_room.SmartRoom.get_instance(room_id)
    logging.info("room_id: " + str(room_id) + " offset: " + str(offset) + " limit: " +str(limit))
    try:
        data = room.get_data(offset, limit)
        ret_val = {"status": "ok", 'room_data': data }
    except smart_room.SmartRoom.RoomNotFoundError as e:
        ret_val = {"status": "ko", 'error': e.error }

    return json.dumps(ret_val)

@app.route("/smart-room/1.0/rooms/<room_id>/data", methods=['POST'])
def post_room_status(room_id):
    logging.info("post_room_status.1")
    ret_val = {}
    try:
        room = smart_room.SmartRoom.get_instance(room_id)
        input = request.get_data()
        logging.info(str(input))
        data = json.loads(input)
        logging.info("room_id: " + str(room_id) + " input data: " + str(data))
        values = room.add_data(data)
        ret_val = {"status": "ok" }

    except smart_room.SmartRoom.RoomNotFoundError as e:
        ret_val = {"status": "ko", 'error': e.error }
    except ValueError:
        ret_val = {"status": "ko", 'error': 'invalid_values' }
    except:
        logging.error(str(sys.exc_info()))
        ret_val = {"status": "ko", 'error': str(sys.exc_info()) }
    return json.dumps(ret_val)

@app.route("/test")
def test():
  return app.send_static_file('test.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0',port=5000)
