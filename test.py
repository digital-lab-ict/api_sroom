import unittest
import sys
import smart_room
import datetime
import time
import logging

logging.basicConfig(level=logging.DEBUG)

user1 = None
domain1 = None
password1 = None
room1 = None

class TestSmartRoom(unittest.TestCase):
    def test_get_room_status(self):
        room = smart_room.SmartRoom.get_instance(room1)
        self.assertFalse(room is None)

        data = room.get_data(0, -1)
        logging.info(str(data))
        self.assertFalse(data is None)

        rooms = smart_room.SmartRoom.get_room_list()
        self.assertFalse(rooms is None)

    def test_set_room_status(self):
        room = smart_room.SmartRoom.get_instance(room1)
        self.assertFalse(room is None)
        data = {'occupata':1, 'temperatura': 25.67, 'luminosita': 34}
        ret_val = room.add_data(data)
        self.assertFalse(ret_val is None)

        data = room.get_data(0, -1)
        logging.info(str(data))
        self.assertFalse(data is None)

if __name__ == '__main__':
  if len(sys.argv) < 4:
    print "usage: " + sys.argv[0] + " user_id domain password room1 room2"
    exit()
  user1 = sys.argv[1]
  domain1 = sys.argv[2]
  password1 = sys.argv[3]
  room1 = sys.argv[4]
  sys.argv = sys.argv[:1]
  unittest.main()
