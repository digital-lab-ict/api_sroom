import logging

class SmartRoom:
    def __init__(self, room_id, name, location):
        self._room_id = room_id
        self._name = name
        self._location = location
        self._data = []

    rooms = {}
    MAX_DATA_SIZE = 10000

    @classmethod
    def get_instance(cls, room_id):
        if cls.rooms.get(room_id) is None:
            cls.rooms[room_id] = SmartRoom(room_id, 'Test Room', 'Via Fabiani')
        try:
            return cls.rooms[room_id]
        except:
            raise self.RoomNotFoundError('room_not_found')

    @classmethod
    def get_room_list(cls):
        return cls.rooms.keys()

    class RoomNotFoundError(Exception):
        def __init__(error):
            self._error = error

        @property
        def error(self):
            return self._error

    def add_data(self, data):
        self._data.insert(0, data)
        if len(self._data) > self.MAX_DATA_SIZE:
            self.data.pop()
        return data

    def get_data(self, offset=0, limit=9999999999):
        offset = min(offset, len(self._data))
        offset = offset if offset else 0
        limit = min(limit, len(self._data) + offset)
        limit = limit if limit > 0 else len(self._data)
        return self._data[offset:limit]
